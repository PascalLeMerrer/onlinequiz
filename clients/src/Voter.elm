port module Voter exposing (Model, init, main, subscriptions, update)

import Browser exposing (Document)
import Colors exposing (..)
import Data.Question exposing (Question, questionDecoder, questionListDecoder, questionListEncoder)
import Data.Vote exposing (voteEncoder)
import Element exposing (Element, alignLeft, alignRight, centerX, centerY, column, fill, height, padding, paddingXY, px, rgb, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input exposing (Option)
import Html.Attributes exposing (disabled)
import Json.Decode
import Json.Encode
import List.Extra


port connect : () -> Cmd msg


port vote : Json.Encode.Value -> Cmd msg


port connected : (Json.Encode.Value -> msg) -> Sub msg


port questionPublished : (Json.Encode.Value -> msg) -> Sub msg


type alias Model =
    { currentQuestion : Maybe Question
    , errorMessage : String
    , hasVoted : Bool
    , selectedOption : Maybe Int
    , status : Status
    , userId : String -- TODO: Maybe String? or Custom type?
    }


type Msg
    = Connected String
    | DecodingError String
    | QuestionPublished Question
    | OptionSelected Int
    | Vote


type alias Quiz =
    { questions : List Question }


type Status
    = ConnectionDone
    | ConnectionFailed
    | ConnectionInProgress
    | ConnectionNotAsked
    | QuestionDecodingFailed


init : () -> ( Model, Cmd Msg )
init _ =
    ( { currentQuestion = Nothing
      , errorMessage = ""
      , hasVoted = False
      , selectedOption = Nothing
      , status = ConnectionInProgress
      , userId = ""
      }
    , connect ()
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Connected userId ->
            ( { model
                | status = ConnectionDone
                , userId = userId
              }
            , Cmd.none
            )

        DecodingError message ->
            ( { model
                | errorMessage = message
                , status = ConnectionFailed
              }
            , Cmd.none
            )

        QuestionPublished question ->
            ( { model
                | currentQuestion = Just question
                , hasVoted = False
                , errorMessage = ""
              }
            , Cmd.none
            )

        Vote ->
            let
                selectedOption =
                    Maybe.withDefault -1 model.selectedOption
            in
            case model.currentQuestion of
                Just question ->
                    ( { model
                        | hasVoted = True
                      }
                    , vote <| voteEncoder { questionId = question.id, optionIndex = selectedOption }
                    )

                Nothing ->
                    ( model, Cmd.none )

        OptionSelected index ->
            ( { model | selectedOption = Just index }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [ connected decodeValue, questionPublished decodeQuestion ]


decodeValue : Json.Encode.Value -> Msg
decodeValue value =
    let
        result =
            Json.Decode.decodeValue Json.Decode.string value
    in
    case result of
        Ok string ->
            Connected string

        Err _ ->
            DecodingError "Invalid user ID received."


decodeQuestion : Json.Encode.Value -> Msg
decodeQuestion value =
    let
        result =
            Json.Decode.decodeValue questionDecoder value
    in
    case result of
        Ok question ->
            QuestionPublished question

        Err _ ->
            DecodingError "Invalid question received."


view : Model -> Document Msg
view model =
    { title = "Online Quiz"
    , body = [ Element.layout [] (mainContent model) ]
    }


mainContent : Model -> Element Msg
mainContent model =
    case model.status of
        ConnectionInProgress ->
            viewWaitingScreen

        ConnectionFailed ->
            viewErrorScreen model

        ConnectionDone ->
            viewQuestion model

        QuestionDecodingFailed ->
            viewErrorScreen model

        -- TODO display specific message?
        ConnectionNotAsked ->
            viewErrorScreen model


viewWaitingScreen : Element Msg
viewWaitingScreen =
    Element.el
        [ Background.color infoLightColor
        , Border.color infoDarkColor
        , Border.rounded 5
        , Font.color infoDarkColor
        , centerY
        , centerX
        , paddingXY 20 10
        ]
        (Element.text "Connection in progress...")


viewErrorScreen : Model -> Element Msg
viewErrorScreen model =
    Element.el
        [ Background.color errorLightColor
        , Border.color errorDarkColor
        , Font.color errorDarkColor
        , centerY
        , centerX
        , paddingXY 20 10
        ]
        (Element.text model.errorMessage)


viewQuestion : Model -> Element Msg
viewQuestion model =
    case model.currentQuestion of
        Nothing ->
            Element.el
                [ centerX
                , centerY
                ]
                (Element.text "Please wait for the first question.")

        Just question ->
            column
                [ centerX
                , centerY
                , spacing 20
                ]
                [ Element.el
                    [ Font.size 20
                    , Font.bold
                    , Font.color darkPrimaryColor
                    ]
                    (Element.text question.label)
                , Input.radio
                    [ padding 10
                    , spacing 20
                    ]
                    { onChange = \selectedOptionIndex -> OptionSelected selectedOptionIndex
                    , selected = model.selectedOption
                    , label = Input.labelAbove [ Font.size 14, paddingXY 0 12 ] (text "options")
                    , options = List.indexedMap viewOption question.options
                    }
                , viewVoteButton model
                ]


viewOption : Int -> String -> Option Int Msg
viewOption index label =
    Input.option index (text label)


viewVoteButton : Model -> Element Msg
viewVoteButton model =
    Input.button
        [ Font.size 16
        , Background.color <|
            if model.hasVoted then
                disabledColor

            else
                primaryColor
        , Font.color white
        , alignRight
        , paddingXY 40 10
        , Border.rounded 5
        ]
        { onPress =
            if model.hasVoted then
                Nothing

            else
                Just Vote
        , label =
            text <|
                if model.hasVoted then
                    "You voted."

                else
                    "Vote"
        }


main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
