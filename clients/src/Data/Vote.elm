module Data.Vote exposing (Vote, voteDecoder, voteEncoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode


type alias Vote =
    { optionIndex : Int
    , questionId : String
    }


voteDecoder : Decoder Vote
voteDecoder =
    Decode.succeed Vote
        |> required "optionIndex" Decode.int
        |> required "questionId" Decode.string


voteEncoder : Vote -> Encode.Value
voteEncoder vote =
    Encode.object
        [
        ( "optionIndex", Encode.int vote.optionIndex )
        , ( "questionId", Encode.string vote.questionId )
        ]
