module Data.Question exposing (Question, questionDecoder, questionListDecoder, questionListEncoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode


type alias Question =
    { id : String
    , kind : String
    , label : String
    , options : List String
    , votes : List Int
    }


questionListDecoder : Decoder (List Question)
questionListDecoder =
    Decode.list questionDecoder


questionDecoder : Decoder Question
questionDecoder =
    Decode.succeed Question
        |> required "id" Decode.string
        |> required "kind" Decode.string
        |> required "label" Decode.string
        |> required "options" (Decode.list Decode.string)
        |> required "votes" (Decode.list Decode.int)


questionListEncoder : List Question -> Encode.Value
questionListEncoder questionList =
    Encode.list questionEncoder questionList


questionEncoder : Question -> Encode.Value
questionEncoder question =
    Encode.object
        [ ( "id", Encode.string question.id )
        , ( "kind", Encode.string question.kind )
        , ( "label", Encode.string question.label )
        , ( "options", Encode.list Encode.string question.options )
        , ( "votes", Encode.list Encode.int question.votes )
        ]
