module Colors exposing (darkPrimaryColor, disabledColor, errorDarkColor, errorLightColor, infoDarkColor, infoLightColor, primaryColor, successDarkColor, successLightColor, white)

import Element exposing (rgb)


primaryColor =
    rgb 0.01 0.651 0.455


white =
    rgb 1 1 1


disabledColor =
    rgb 0.5 0.5 0.5


darkPrimaryColor =
    rgb 0 0.22 0.25


successLightColor =
    rgb 0.294 0.741 0.608


successDarkColor =
    rgb 0 0.396 0.275


errorLightColor =
    rgb 0.941 0.373 0.494


errorDarkColor =
    rgb 0.557 0 0.122


infoLightColor =
    rgb 0.325 0.518 0.733


infoDarkColor =
    rgb 0.24 0.196 0.392
