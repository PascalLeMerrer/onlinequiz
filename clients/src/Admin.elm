port module Admin exposing (Model, init, main, subscriptions, update)

import Browser exposing (Document)
import Colors exposing (..)
import Data.Question exposing (Question, questionDecoder, questionListDecoder, questionListEncoder)
import Element exposing (Element, alignLeft, alignRight, centerX, centerY, column, fill, height, paddingXY, px, rgb, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Json.Decode
import Json.Encode
import Keyboard exposing (Key(..), KeyChange(..), RawKey)
import List.Extra


port connect : String -> Cmd msg


port sendQuiz : Json.Encode.Value -> Cmd msg


port goToNextQuestion : () -> Cmd msg


port connected : (Json.Encode.Value -> msg) -> Sub msg


port votesUpdated : (Json.Encode.Value -> msg) -> Sub msg


type alias Model =
    { currentQuestionIndex : Int
    , errorMessage : String
    , password : String
    , quiz : Maybe Quiz
    , status : Status
    , userId : String -- TODO: Maybe String? or Custom type?
    }


type Msg
    = Connect
    | Connected String
    | DecodingError String
    | KeyUp RawKey
    | NextQuestion
    | PasswordChanged String
    | SubmitQuiz
    | QuizChanged String
    | VoteUpdated Question


type alias Quiz =
    { questions : List Question }


type Status
    = Authenticated
    | ConnectionFailed
    | ConnectionInProgress
    | ConnectionNotAsked
    | QuizDecodingFailed
    | InvalidVote


init : () -> ( Model, Cmd Msg )
init _ =
    ( { currentQuestionIndex = -1
      , errorMessage = ""
      , password = ""
      , quiz = Nothing
      , status = ConnectionNotAsked
      , userId = ""
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Connect ->
            ( { model
                | errorMessage = ""
                , status = ConnectionInProgress
              }
            , connect model.password
            )

        Connected userId ->
            ( { model
                | status = Authenticated
                , userId = userId
              }
            , Cmd.none
            )

        DecodingError message ->
            ( { model
                | errorMessage = message
                , status = ConnectionFailed
              }
            , Cmd.none
            )

        KeyUp rawKey ->
            case Keyboard.anyKeyOriginal rawKey of
                Just Enter ->
                    if model.status == ConnectionNotAsked then
                        update Connect model

                    else
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        NextQuestion ->
            ( { model
                | currentQuestionIndex = model.currentQuestionIndex + 1
              }
            , goToNextQuestion ()
            )

        PasswordChanged newPassword ->
            ( { model | password = newPassword }, Cmd.none )

        QuizChanged string ->
            let
                decodedJson =
                    Json.Decode.decodeString questionListDecoder string
            in
            case decodedJson of
                Ok questions ->
                    let
                        quiz =
                            { questions = questions }
                    in
                    ( { model
                        | quiz = Just quiz
                        , currentQuestionIndex = 0
                        , errorMessage = ""
                      }
                    , sendQuiz (questionListEncoder quiz.questions)
                    )

                Err _ ->
                    ( { model
                        | errorMessage = "Cannot decode Json for questions"
                        , status = QuizDecodingFailed
                      }
                    , Cmd.none
                    )

        SubmitQuiz ->
            ( { model | currentQuestionIndex = 0 }, Cmd.none )

        VoteUpdated question ->
            case model.quiz of
                Just quiz ->
                    let
                        newQuestions =
                            List.Extra.setAt model.currentQuestionIndex question quiz.questions

                        newQuiz =
                            { questions = newQuestions }
                    in
                    ( { model | quiz = Just newQuiz }, Cmd.none )

                Nothing ->
                    ( { model
                        | errorMessage = "Vote received, but quiz is not in progress."
                        , status = InvalidVote
                      }
                    , Cmd.none
                    )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ connected decodeUserId
        , Keyboard.ups KeyUp
        , votesUpdated decodeVote
        ]


decodeUserId : Json.Encode.Value -> Msg
decodeUserId value =
    let
        result =
            Json.Decode.decodeValue Json.Decode.string value
    in
    case result of
        Ok string ->
            Connected string

        Err _ ->
            DecodingError "Invalid user ID received."


decodeVote : Json.Encode.Value -> Msg
decodeVote vote =
    let
        result =
            Json.Decode.decodeValue questionDecoder vote
    in
    case result of
        Ok question ->
            VoteUpdated question

        Err _ ->
            DecodingError "Invalid vote received."


view : Model -> Document Msg
view model =
    { title = "Admin"
    , body = [ Element.layout [] (mainContent model) ]
    }


mainContent : Model -> Element Msg
mainContent model =
    case model.status of
        ConnectionNotAsked ->
            viewConnectionForm model

        ConnectionInProgress ->
            viewWaitingScreen

        Authenticated ->
            viewQuizScreen model

        _ ->
            viewErrorScreen model


viewConnectionForm : Model -> Element Msg
viewConnectionForm model =
    column
        [ centerX
        , centerY
        , spacing 20
        ]
        [ Input.newPassword
            [ paddingXY 5 5
            , Font.size 16
            , centerY
            ]
            { onChange = \string -> PasswordChanged string
            , text = model.password
            , placeholder = Just (Input.placeholder [] (text "password"))
            , label =
                Input.labelLeft
                    [ Font.size 16
                    , Font.color darkPrimaryColor
                    , centerY
                    , paddingXY 20 0
                    ]
                    (text "Admin Password:")
            , show = False
            }
        , viewPrimaryButton Connect "Connect"
        ]


viewWaitingScreen : Element Msg
viewWaitingScreen =
    Element.el
        [ Background.color infoLightColor
        , Border.color infoDarkColor
        , Border.rounded 5
        , Font.color infoDarkColor
        , centerY
        , centerX
        , paddingXY 20 10
        ]
        (Element.text "Connection in progress...")


viewErrorScreen : Model -> Element Msg
viewErrorScreen model =
    Element.el
        [ Background.color errorLightColor
        , Border.color errorDarkColor
        , Font.color errorDarkColor
        , centerY
        , centerX
        , paddingXY 20 10
        ]
        (Element.text model.errorMessage)


viewQuizScreen : Model -> Element Msg
viewQuizScreen model =
    case model.quiz of
        Nothing ->
            viewQuizCreationForm model

        Just quiz ->
            viewQuestionAt model.currentQuestionIndex quiz.questions


viewQuizCreationForm : Model -> Element Msg
viewQuizCreationForm model =
    column
        [ centerX
        , centerY
        , spacing 20
        ]
        [ Input.multiline
            [ paddingXY 5 5
            , Font.size 16
            , centerY
            , width <| px 700
            , height <| px 600
            ]
            { onChange = \string -> QuizChanged string
            , text = ""
            , placeholder = Nothing
            , label =
                Input.labelAbove
                    [ Font.size 16
                    , Font.color darkPrimaryColor
                    ]
                    (text "Quiz description in Json format:")
            , spellcheck = False
            }
        , viewPrimaryButton SubmitQuiz "Submit"
        ]


viewPrimaryButton : Msg -> String -> Element Msg
viewPrimaryButton msg label =
    Input.button
        [ Font.size 16
        , Background.color primaryColor
        , Font.color white
        , alignRight
        , paddingXY 40 10
        , Border.rounded 5
        ]
        { onPress = Just msg
        , label = text label
        }


viewQuestionAt : Int -> List Question -> Element Msg
viewQuestionAt index questions =
    let
        currentQuestion =
            List.Extra.getAt index questions
    in
    case currentQuestion of
        Nothing ->
            Element.el
                [ centerX
                , centerY
                ]
                (Element.text "It's finished!")

        Just question ->
            column
                [ centerX
                , centerY
                , spacing 20
                ]
                [ Element.el
                    [ Font.size 40
                    , Font.bold
                    , Font.color darkPrimaryColor
                    ]
                    (Element.text question.label)
                , column [ spacing 20 ] <|
                    List.map viewOption (List.Extra.zip question.options question.votes)
                , viewPrimaryButton NextQuestion "Next Question"
                ]


viewOption : ( String, Int ) -> Element Msg
viewOption ( label, votes ) =
    column []
        [ Element.el
            [ Font.size 36 ]
            (Element.text label)
        , row []
            [ Element.el
                [ width <| px (votes * 10)
                , height <| px 36
                , Background.color primaryColor
                ]
                Element.none
            , Element.el
                [ Font.size 30 ]
                (if votes > 0 then
                    Element.text <| String.fromInt votes

                 else
                    Element.none
                )
            ]
        ]


main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
