module AdminTest exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


suite : Test
suite =
    describe "The Admin module"
        [ describe "Useless test"
            -- Nest as many descriptions as you like.
            [ skip <|
                test "has no effect on a palindrome" <|
                    \_ ->
                        Expect.equal True True
            ]
        ]
