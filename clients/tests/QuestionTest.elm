module QuestionTest exposing (suite)

import Data.Question exposing (Question, questionListDecoder, questionListEncoder)
import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Json.Decode
import Json.Encode
import Test exposing (..)


suite : Test
suite =
    describe "The question module"
        [ describe "should provide a decoder for JSON quiz description"
            [ test "which decodes valid Json" <|
                \_ ->
                    let
                        decodedJson =
                            Json.Decode.decodeString questionListDecoder quizJson
                    in
                    Expect.ok decodedJson
            ]
        , describe "should provide an encoder for questions"
            [ test "which produces Json" <|
                \_ ->
                    let
                        encodedJson =
                            Json.Encode.encode 4 <| questionListEncoder quiz
                    in
                    Expect.equal quizJson encodedJson
            ]
        ]


question1 : Question
question1 =
    { id = "Q1"
    , kind = "multipleChoices" -- TODO use const
    , label = "What is the color of the white horse of Henri IV?"
    , options =
        [ "black"
        , "white"
        , "Can you repeat the question?"
        ]
    , votes = []
    }


quiz =
    [ question1 ]


quizJson =
    """[
    {
        "id": "Q1",
        "kind": "multipleChoices",
        "label": "What is the color of the white horse of Henri IV?",
        "options": [
            "black",
            "white",
            "Can you repeat the question?"
        ],
        "votes": []
    }
]"""
