package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"

	"github.com/speps/go-hashids"

	"time"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"golang.org/x/net/websocket"
)

// Question is a quiz question
type Question struct {
	ID      string   `json:"id"`
	Kind    string   `json:"kind"` // the object type
	Label   string   `json:"label"`
	Options []string `json:"options"`
	Votes   []int    `json:"votes"`
	Voters  []*User  `json:"-"` // omit from json
}

//type Quiz struct {
//	Questions []Question
//}

// Action represents a user action
type Action struct {
	QuestionID string `json:"questionId"`
	Type       string `json:"type"` // the action type
	UserId     string `json:"userId"`
	Payload    string `json:"payload"`
	// Payload depends on the action type:
	// * for joining as admin it's the password
	// * for voting for a multiple choice question it's the index of the selected option
	// * for voting for a word cloud, it's the selected word
	// * for starting a quiz it's the description of questions as a json string
}

// User represents a user, who may be the admin or a voter
type User struct {
	ID              string          `json:"id"`
	CurrentQuestion *Question       `json:"currentQuestion"`
	lastPing        time.Time       `json:"-"` // omit
	socket          *websocket.Conn `json:"-"` // omit
	isDisconnected  bool            `json:"-"` // omit
}

type Vote struct {
	User       *User
	QuestionID string
	Answer     string
}

type ConnectionAnswer struct {
	Type   string `json:"type"`
	UserId string `json:"userId"`
}

type QuestionPublished struct {
	Type     string    `json:"type"`
	Question *Question `json:"question"`
}

var questions []*Question
var currentQuestion *Question
var currentQuestionIndex int
var isQuizFinished bool
var users []*User
var sockets []*websocket.Conn
var logger echo.Logger
var votingChannel chan Vote
var admin User

// KEEP_ALIVE_TIMEOUT_MS is the max time between two reception of a keep alive message from a given client
const KEEP_ALIVE_TIMEOUT_MS = 10000
// CLEANING_INTERVAL_MS the interval at which dead connexion are removed
const CLEANING_INTERVAL_MS = time.Millisecond * 1000
// CLIENT_UPDATE_INTERVAL_MS the interval at which each goroutine in charge of a client checks if the quiz is finished
const CLIENT_UPDATE_INTERVAL_MS = 1000

// action types
const GO_TO_NEXT_QUESTION = "goToNextQuestion"
const JOIN = "join"
const JOIN_AS_ADMIN = "joinAsAdmin"
const KEEP_ALIVE = "keepAlive"
const START_QUIZ = "startQuiz"
const VOTE = "vote"

// Client message types
const CONNECTED = "connected"
const QUESTION_PUBLISHED = "questionPublished"
const VOTES_UPDATED = "votesUpdated"

// quiz types
const MULTIPLE_CHOICES = "multipleChoices"
const WORD_CLOUD = "wordCloud"

// voting channel configuration
const VOTING_BUFFER_SIZE = 100

func receiveActions(ws *websocket.Conn, channel chan Vote) error {
	var err error
	for {

		action := new(Action)
		err = websocket.JSON.Receive(ws, &action)
		if err != nil {
			logger.Error(fmt.Sprintf("Receive error: %s\n", err))
			break
		}

		logger.Debug(fmt.Sprintf("Received action %+v\n", action))

		switch action.Type {
		case JOIN:
			user := join(ws, action)
			sendQuestion(user, QUESTION_PUBLISHED)
		case JOIN_AS_ADMIN:
			joinAsAdmin(ws, action, channel)
		case START_QUIZ:
			startQuiz(ws, action)
		case VOTE:
			submitVote(ws, action, channel)
		case GO_TO_NEXT_QUESTION:
			goToNextQuestion()
		case KEEP_ALIVE:
			keepAlive(action)
		default:
			logger.Error(fmt.Sprintf("Unknown action type: %s\n", action.Type))
		}
	}
	_ = ws.Close()
	return err
}

func startQuiz(ws *websocket.Conn, action *Action) {
	if action.Payload == "" {
		logger.Error("Questions description is missing, quiz cannot start\n")
		return
	}
	err := json.Unmarshal([]byte(action.Payload), &questions)

	if err != nil {
		logger.Error(fmt.Sprintf("Invalid Quiz description: %s\n", action.Payload))
	}

	currentQuestionIndex = -1

	initVotes()

	goToNextQuestion()
}

func initVotes() {

	for _, question := range questions {
		question.Votes = make([]int, len(question.Options))
	}
	fmt.Printf("questions after init %+v\n", questions[0])
}

func goToNextQuestion() {
	currentQuestionIndex++

	if currentQuestionIndex == len(questions) {
		fmt.Println("Quiz finished")
		isQuizFinished = true
		return
	}

	currentQuestion = questions[currentQuestionIndex]
	fmt.Printf("currentQuestion %+v\n", currentQuestion)

	for _, user := range users {
		sendQuestion(user, QUESTION_PUBLISHED)
	}
}

func joinAsAdmin(ws *websocket.Conn, action *Action, channel chan Vote) {
	adminPassword := os.Getenv("PASSWORD")
	if adminPassword == "" {
		logger.Error("Admin password is not configured\n")
	}
	if action.Payload != adminPassword {
		logger.Error("Trying to connect with invalid password\n")
		return
	}
	admin = User{
		generateID(),
		nil,
		time.Now(),
		ws,
		false,
	}
	sendConnectedAnswer(&admin, ws)

	reset()
	go listenToVotes(channel)
}

func reset() {
	currentQuestionIndex = -1
	currentQuestion = nil
}

func listenToVotes(channel chan Vote) {
	for {
		vote := <-channel
		validateVote(vote)
		// TODO quit quizz when finished; select on multiple channels?
	}
}

func sendQuestion(user *User, kind string) {
	if currentQuestion == nil {
		logger.Debug("Quiz not started yet")
		return
	}

	publishedQuestion := QuestionPublished{
		Type:     kind,
		Question: currentQuestion,
	}

	sendError := websocket.JSON.Send(user.socket, publishedQuestion)
	if sendError != nil {
		logger.Error(fmt.Printf("Cannot send question: %s\n", sendError))
		user.isDisconnected = true

	}
}

// submitVote for the current question
func submitVote(ws *websocket.Conn, action *Action, channel chan Vote) {
	switch currentQuestion.Kind {
	case MULTIPLE_CHOICES:

		user := getUser(ws)

		vote := Vote{
			User:       user,
			QuestionID: action.QuestionID,
			Answer:     action.Payload,
		}
		channel <- vote

	case WORD_CLOUD:
		// TODO process word cloud vote
	default:
		logger.Error(fmt.Sprintf("Invalid question type %s\n", currentQuestion.Kind))
	}
}

func validateVote(vote Vote) {
	index, err := strconv.Atoi(vote.Answer)
	if err != nil {
		logger.Error(fmt.Sprintf("Invalid vote for QCM", vote.Answer))
	}
	if isValidVote(index, vote) {
		currentQuestion.Votes[index]++
		currentQuestion.Voters = append(currentQuestion.Voters, vote.User)
		logger.Debug(fmt.Sprintf("Votes for QCM", currentQuestion.Votes))
		sendQuestion(&admin, VOTES_UPDATED)
	}
}

func isValidVote(index int, vote Vote) bool {
	for _, voter := range currentQuestion.Voters {
		if voter == vote.User {
			return false
		}
	}
	isValidOptionIndex := index > 0 && index < len(currentQuestion.Options)
	isValidQuestionId := vote.QuestionID == currentQuestion.ID
	return isValidOptionIndex && isValidQuestionId
}

func getUser(ws *websocket.Conn) *User {
	for _, user := range users {
		if user.socket == ws {
			return user
		}
	}
	return nil
}

// invoked when a new user joins the quiz
func join(ws *websocket.Conn, action *Action) *User {
	user := User{
		generateID(),
		nil,
		time.Now(),
		ws,
		false,
	}
	users = append(users, &user)
	logger.Info(fmt.Sprintf("User %s joined the quiz\n", user.ID))

	sendConnectedAnswer(&user, ws)

	return &user
}

func sendConnectedAnswer(user *User, ws *websocket.Conn) {
	answer := ConnectionAnswer{
		Type:   CONNECTED,
		UserId: user.ID,
	}

	sendError := websocket.JSON.Send(ws, answer)
	if sendError != nil {
		user.isDisconnected = true
	}

}

// maintains a given user in the list of active users
// if the user does not invoke keepAlive at least once per time period,
// he/she is kicked off from the user list
// it allows detecting the disconnection of clients
// or those with a poor connection
func keepAlive(action *Action) {
	for _, user := range users {
		if user.ID == action.UserId {
			user.lastPing = time.Now()
		}
	}
}

func removeDisconnectedUsers() {
	now := time.Now()
	for i, user := range users {
		if user.isDisconnected || now.Sub(user.lastPing) > KEEP_ALIVE_TIMEOUT_MS*time.Millisecond {
			logger.Error(fmt.Sprintf("kicking playing %s, inactive since %v \n", user.ID, user.lastPing))
			removeSocket(user.socket)
			if len(users) > 1 {
				users = append(users[:i], users[i+1:]...)

			} else {
				users = make([]*User, 0)
			}
		}
	}
}

func removeSocket(ws *websocket.Conn) {
	for index, socket := range sockets {
		if socket == ws {
			_ = socket.Close()
			if len(sockets) > 1 {
				sockets = append(sockets[:index], sockets[index+1:]...)
			} else {
				sockets = make([]*websocket.Conn, 0)
			}
		}
	}
}

// removes the disconnected users from the user list
func clean() {
	for {
		removeDisconnectedUsers()
		time.Sleep(CLEANING_INTERVAL_MS)
	}
}

// returns an 8 characters random string
// the characters may be any of [A-Z][a-z][0-9]
func generateID() string {
	hashIDConfig := hashids.NewData()
	hashIDConfig.Salt = "zs4e6f80KDla1-2xcCD!34%<?23POsd"
	hashIDConfig.MinLength = 8
	hashIDConfig.Alphabet = hashids.DefaultAlphabet
	hash, _ := hashids.NewWithData(hashIDConfig)

	randomInt := rand.New(rand.NewSource(time.Now().UnixNano())).Int63()
	intArray := intToIntArray(randomInt, 8)
	result, _ := hash.Encode(intArray)

	return result
}

// converts an int64 number to a fixed length array of int
func intToIntArray(value int64, length int) []int {
	result := make([]int, length)
	valueAsString := strconv.FormatInt(value, 10)

	fragmentLength := len(valueAsString) / length

	var startIndex, endIndex int
	var intValue int64
	var err error

	for index := 0; index < length; index++ {

		startIndex = index * fragmentLength
		endIndex = ((index + 1) * fragmentLength)

		if endIndex <= len(valueAsString) {
			intValue, err = strconv.ParseInt(valueAsString[startIndex:endIndex], 10, 0)
		} else {
			intValue, err = strconv.ParseInt(valueAsString[startIndex:], 10, 0)
		}

		if err != nil {
			log.Panicf("Error while converting string to int array %s\n", err)
		}
		result[index] = int(intValue)
	}
	return result
}

func createWebSocket(c echo.Context) error {
	websocket.Handler(func(ws *websocket.Conn) {
		defer ws.Close()
		c.Logger().Debug("Creating a new websocket")
		sockets = append(sockets, ws)
		go receiveActions(ws, votingChannel)
		for {
			if isQuizFinished {
				break
			}
			time.Sleep(time.Millisecond * CLIENT_UPDATE_INTERVAL_MS)
		}
	}).ServeHTTP(c.Response(), c.Request())
	return nil
}

func main() {
	e := echo.New()

	logger = e.Logger
	logger.Info("Server starting, will listen on port 1323")

	e.Use(mw.Logger())
	e.Use(mw.Recover())

	e.Static("/", "public")
	e.GET("/ws", createWebSocket)

	votingChannel = make(chan Vote, VOTING_BUFFER_SIZE)

	go clean()

	logger.Fatal(e.Start(":1323"))
}
