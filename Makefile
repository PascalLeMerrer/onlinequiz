build: buildserver buildclients

buildclients: clients/src/*.elm clients/src/Data/*.elm
	cd clients && elm make src/Admin.elm --output=../public/admin.js --debug
	cd clients && elm make src/Voter.elm --output=../public/voter.js --debug


buildserver: src/server.go
	go build src/server.go


livevoter:
	cd clients && elm-live src/Voter.elm -- --output=../public/voter.js --debug